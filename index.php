<?php

//hacemos la peticion GET a la pai y decodificamos EN json
$data= json_decode(file_get_contents('https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region=7'),true);


//abrimos la session curl
$ch = curl_init();
 
// definimos la URL a la que hacemos la petición
curl_setopt($ch, CURLOPT_URL,"https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones");

//indicamos el header
curl_setopt($ch, CURLOPT_HEADER, "Content-Type: multipart/form-data");
// indicamos el tipo de petición: POST
curl_setopt($ch, CURLOPT_POST, TRUE);
// definimos cada uno de los parámetros
curl_setopt($ch, CURLOPT_POSTFIELDS, "reg_id=7");
 
// recibimos la respuesta y la guardamos en una variable
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$remote_server_output = curl_exec ($ch);
 
// cerramos la sesión cURL
curl_close ($ch);
 


?>
<html>

<head>
    <title>Test</title>
    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Test, erika, developer" />
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- //Meta Tags -->
    <!-- font-awesome-stylesheet -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- //font-awesome-stylesheet -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <!--fonts-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
    <!--//fonts-->
	    <!--jquery-->
	<script src="js/jquery.min.js"></script>
	    <!--//jQuery-->
</head>

<body>
    <!-- main-content -->
    <h1>Test Erika</h1>
    <div class="main-content-agileits">
        <!-- left-content -->
      
        <!-- //left-content -->
        <!-- buscado -->
        <div class="login-w3l">
            <div class="top-img-agileits-w3layouts">
                <div class="w3_pad">

                    <h2 class="sub-head-w3-agileits">Buscador de Farmacias </h2>
                    <p> Encuentra si tu farmacia esta disponible por comuna o Nombre del local</p>

                    <div class="login-form">
                        <form action="#" method="post">
                            <div class="w3_lb">
                                <label>Comuna</label>
                            </div>
							<!-- listado de comunas   -->
							<div class="w3_lb"><select name="comuna" id="comuna" class="select-css" onChange="buscar_f(this.value);">
								<?php print_r($remote_server_output); ?></select></div>
                             <div class="w3_lb">
                                <label>Nombre Local</label>
                            </div>
							<!-- listado de nombres   -->
							<div class="w3_lb">
								
							<select name="nombre" id="nombre" class="select-css" onChange="buscar_fn(this.value);">
								<option value="0">Elija Nombre</option>
								
								<?php
								//recorrido del array de farmacias
								for($i=0; $i<count($data); $i++){
							
									?>
								<option value="<?php echo $data[$i]['local_nombre']; ?>"><?php echo $data[$i]['local_nombre']; ?></option>
								<?php

								}
								?>
								
								</select>
							</div>
                          
                        </form>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="w3_soc">
                   <button type="button" onClick="resetear();" class="btn">Reiniciar</button>
                </div>
            </div>
        </div>
		  <!-- buscado -->
		  <div class="left-text-w3ls">
            <!-- div que muestra las farmacias -->
           <div  id="v_farmacias">
			   <img src="img/ubicacion.png">
			  </div>
                <!-- //div que muestra las farmacias -->
        </div>
        <div class="clear"></div>
    </div>
   
		</div>


    <!--// main-content -->
    <!--Copyright-->
    <div class="footer-agileits">
        <p>© 2020 buscador. All Rights Reserved | Design by Erika Lugo</p>
    </div>
    <!--//Copyright-->
	<script>
		//funcion para filtrar las farmacias por comunas utilizando ajax
 function buscar_f(comuna) {
	 
	 var nombre= $('#nombre').val();
	 if(nombre!='0'){
		  var pars = 'tipo=3&comuna=' + comuna + '&nombre='+ nombre;
	 }else{
		  var pars = 'tipo=1&comuna=' + comuna;
	 }
   var url = 'ajax/farmacias.php';
 
  
   $.ajax({
            type: "POST",
            url: url,
           
            data: pars,
            success: function(data) {
               $('#v_farmacias').html(data);
				
      
            }
        }); 
  
}
		//funcion para filtrar las farmacias por nombres utilizando ajax
		 function buscar_fn(nombre) {
			 var comuna=$('#comuna').val();
			 if(comuna!='0'){
				  var pars = 'tipo=3&comuna=' + comuna + '&nombre='+ nombre;
			 }else{
				  var pars = 'tipo=2&nombre=' + nombre;
			 }
			 var url = 'ajax/farmacias.php';
			
   $.ajax({
            type: "POST",
            url: url,
           
            data: pars,
            success: function(data) {
               $('#v_farmacias').html(data);
				
      
            }
        }); 
  
}
	function resetear() {
		
		$('#comuna').prop('selectedIndex',0);
		$('#nombre').prop('selectedIndex',0);
		
		var pars ='tipo=0';
			
			 var url = 'ajax/farmacias.php';
		$.ajax({
            type: "POST",
            url: url,
           
            data: pars,
            success: function(data) {
               $('#v_farmacias').html(data);
				
      
            }
        }); 
		
		
	}
</script>
</body>

</html>
