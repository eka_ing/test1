<?php
//aumento tiempo de ejecucion del php
ini_set('max_execution_time', 3000); 


//cosmimos la api por get
$data= json_decode(file_get_contents('https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region=7'),true);


//definimos la zona horaria
date_default_timezone_set("America/Santiago");


$fecha_actual = strtotime(date("d-m-Y H:i:00",time()));

$html='';
$comuna=$_POST['comuna'];
$nombre=$_POST['nombre'];
$tipo=$_POST['tipo'];


//recorrido del array de farmacias

$exite=0;
for($i=0; $i<count($data); $i++){
	
	$hora1=explode(' ',$data[$i]['funcionamiento_hora_apertura']);
	$hora2=explode(' ',$data[$i]['funcionamiento_hora_cierre']);
	$fecha_inicio=strtotime($data[$i]['fecha']." ".$hora1[0].":00");
	$fecha_fin=strtotime($data[$i]['fecha']." ".$hora2[0].":00");

	//busqueda por comunas
if($tipo==1){

	
	if($comuna==$data[$i]['fk_comuna']){// existe comuna
		
		if(($fecha_actual>=$fecha_inicio)&& ($fecha_actual<=$fecha_fin)){// esta disponible en este horario actual
		$exite=1;	// existe al menos 1 famarmacia en el resultado
?>


			<!--iten farmacia -->
			   <div class="col-2">
				   <h1 class="til"><?php echo $data[$i]['local_nombre']; ?></h1>
				     <h3 class="til2"><?php echo $data[$i]['local_direccion'].', '.$data[$i]['comuna_nombre']; ?></h3>
				    <h3 class="til2"><?php echo $data[$i]['local_telefono']; ?></h3>
				    <h3 class="til2">Desde <?php echo $data[$i]['funcionamiento_hora_apertura']; ?> a <?php echo $data[$i]['funcionamiento_hora_cierre']; ?></h3>
			   <div class="map">
				 <iframe frameborder="0"  height="200" style="border:0"
src="https://www.google.com/maps/embed/v1/search?q=<?php echo $data[$i]['local_lat']; ?>,<?php echo $data[$i]['local_lng']; ?>&key=AIzaSyA8XaLB9bd-kCTjrELSSIGY0y8EEgCgPWY" allowfullscreen></iframe>
				   </div>
			   </div>
			      	<!-- /iten farmacia -->
	
	<?php
			
		}// esta disponible en este horario actual
		
	}// existe comuna

}//tipo 1
	
				//busqueda por nombre 
	if($tipo==2){
	
	if($nombre==$data[$i]['local_nombre']){// existe nombre
			
		if(($fecha_actual>=$fecha_inicio)&& ($fecha_actual<=$fecha_fin)){// esta disponible en este horario actual
			$exite=1;// existe al menos 1 famarmacia en el resultado
?>


		<!--iten farmacia -->
			   <div class="col-2">
				   <h1 class="til"><?php echo $data[$i]['local_nombre']; ?></h1>
				     <h3 class="til2"><?php echo $data[$i]['local_direccion'].', '.$data[$i]['comuna_nombre']; ?></h3>
				    <h3 class="til2"><?php echo $data[$i]['local_telefono']; ?></h3>
				    <h3 class="til2">Desde <?php echo $data[$i]['funcionamiento_hora_apertura']; ?> a <?php echo $data[$i]['funcionamiento_hora_cierre']; ?></h3>
			   <div class="map">
				 <iframe frameborder="0"  height="200" style="border:0"
src="https://www.google.com/maps/embed/v1/search?q=<?php echo $data[$i]['local_lat']; ?>,<?php echo $data[$i]['local_lng']; ?>&key=AIzaSyDYbUVE8CTQ15YfB-klg_brQKHP21GudTs" allowfullscreen></iframe>
				   </div>
			   </div>
			      	<!-- /iten farmacia -->
	
	<?php
			
		} // esta disponible en este horario actual
		
	}// existe nombre

}//tipo 2
	
	
		//busqueda por nombre y comuna
	if($tipo==3){
	
		if($comuna==$data[$i]['fk_comuna']){// existe comuna

	if($nombre==$data[$i]['local_nombre']){// existe nombre
			
		if(($fecha_actual>=$fecha_inicio)&& ($fecha_actual<=$fecha_fin)){// esta disponible en este horario actual
			$exite=1;// existe al menos 1 famarmacia en el resultado
?>


		<!--iten farmacia -->
			   <div class="col-2">
				   <h1 class="til"><?php echo $data[$i]['local_nombre']; ?></h1>
				     <h3 class="til2"><?php echo $data[$i]['local_direccion'].', '.$data[$i]['comuna_nombre']; ?></h3>
				    <h3 class="til2"><?php echo $data[$i]['local_telefono']; ?></h3>
				    <h3 class="til2">Desde <?php echo $data[$i]['funcionamiento_hora_apertura']; ?> a <?php echo $data[$i]['funcionamiento_hora_cierre']; ?></h3>
			   <div class="map">
				 <iframe frameborder="0"  height="200" style="border:0"
src="https://www.google.com/maps/embed/v1/search?q=<?php echo $data[$i]['local_lat']; ?>,<?php echo $data[$i]['local_lng']; ?>&key=AIzaSyDYbUVE8CTQ15YfB-klg_brQKHP21GudTs" allowfullscreen></iframe>
				   </div>
			   </div>
			      	<!-- /iten farmacia -->
	
	<?php
			
		}// esta disponible en este horario actual
		
	}// existe nombre
		}// existe comuna
}//tipo 3
	

	
}//for
if($tipo!=0){// no resetear
 if($exite==0){// no existe ninguna famarmacia en el resultado
	echo '<h1>No hay farmacias Disponibles a Esta Hora</h1>';
 }
}else{// tipo resetear
	
	echo'<img src="img/ubicacion.png">';
}

?>